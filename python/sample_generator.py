# coding=utf-8
import random

for n in range(0, 25):
    print str(random.randint( 25, 35)) + " " + str(random.randint(-25, -5)) + ";"
    print str(random.randint( 45, 60)) + " " + str(random.randint( 35, 50)) + ";"
    print str(random.randint( -5, 10)) + " " + str(random.randint(  5, 15)) + ";"
    # Zufälliges Rauschen
    print str(random.randint(-50, 50)) + " " + str(random.randint(-70, 70)) + ";"
