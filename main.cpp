#include <gtkmm/application.h>
#include "src/hauptmenu.h"
#include "src/nadalib/nadalib.h"
#undef main

int main() {
    Nadalib::init(600, 600, "OpenGL Window", false);
    
    Glib::RefPtr<Gtk::Application> prog = Gtk::Application::create("nada.dlib_praesi");
    Hauptmenu hauptmenu;
    prog->run(hauptmenu);

    Nadalib::close();
    return 0;
}