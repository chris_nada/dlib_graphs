//
// Created by krizchri on 13.09.2017.
//

#pragma once

#include <iostream>
#include <dlib/svm.h>
#include "ki.h"


/**
 * Maschinenlerntyp:
 * Regression: online Regression. Keine Trainingsmethode erforderlich,
 * das Training geschieht beim Hinzufügen von Lerndaten.
 *
 * @tparam sample_type Datentyp der Werte aus der Datenmatrix. In aller Regel ``double``.
 * @tparam spalten Anzahl der Spalten in der Datenmatrix.
 * @tparam zeilen Anzahl der Zeilen in der Datenmatrix.
 */
template<typename sample_type, const long spalten, const long zeilen>
class KI_R_KRLS {

public:

    /**
     * ctor
     * @param gamma Gammawert für die KI.
     */
    explicit KI_R_KRLS(const double gamma = 0.001)
            : krls(dlib::radial_basis_kernel<dlib::matrix<sample_type, spalten, zeilen>>(0.1), gamma) {
    }

    /**
     * Fügt der KI einen weiteren Satz Lerndaten hinzu. Es wird keine Methode train() ausgeführt,
     * da das Lernen 'online' beim Hinzufügen neuer Daten geschieht.
     * @param sample Ordinate
     * @param target Abszisse
     */
    void add_learn_data(const dlib::matrix<sample_type, spalten, zeilen> sample, const sample_type target) {
        krls.train(sample, target);
    };

    /**
     * Vorhersage treffen zum gegebenem Datensatz.
     * @param sample Typ dafür ist ``dlib::matrix<double, n spalten, m zeilen>``
     * @return Wert der Vorhersage als double.
     */
    const sample_type predict(const dlib::matrix<sample_type, spalten, zeilen> sample) {
        return krls(sample);
    }


private:

    /**
     * Interner KI-Algorithmus (Typ)
     */
    dlib::krls<dlib::radial_basis_kernel<dlib::matrix<sample_type, spalten, zeilen>>> krls;

};


