//
// Created by krizchri on 13.09.2017.
//

#pragma once

#include <iostream>
#include <vector>
#include <dlib/svm.h>
#include "ki.h"

/**
 * Maschinenlerntyp:
 * Regressionsanalyse: Kernel ridge regression
 *
 * @tparam sample_type Datentyp der Werte aus der Datenmatrix. In aller Regel ``double``.
 * @tparam spalten Anzahl der Spalten in der Datenmatrix.
 * @tparam zeilen Anzahl der Zeilen in der Datenmatrix.
 */
template<typename sample_type, const long spalten, const long zeilen>
class KI_R_KRR : private KI {

    typedef dlib::radial_basis_kernel<dlib::matrix<sample_type, spalten, zeilen>>  kernel;  //Kernel


public:

    /**
     * ctor
     */
    KI_R_KRR() {
        //Default gamma einstellen. Vorgeschlagen ist: 1.0/(mittlere quadratische Distanz aller punkte)
        const double gamma = 3.0 / dlib::compute_mean_squared_distance(dlib::randomly_subsample(samples, 2000));
        std::cout << "gamma=" << gamma << std::endl;
        trainer.set_kernel(kernel(gamma));
    }

    /**
     * Legt Gamma fest.
     */
    void set_gamma(const double gamma) { trainer.set_kernel(kernel(gamma)); }

    /**
     * Trainingsdaten hinzufügen. sample = Ordinate, target = abhängige Variable.
     */
    void add_learn_data(const dlib::matrix<sample_type, spalten, zeilen> sample, const sample_type target) {
        samples.push_back(sample);
        targets.push_back(target);
    }

    /**
     * Training mit aktuellem Korpus durchführen
     */
    void train() override {
        std::cout << "KI_R_KRR.train()" << std::endl;
        decision_function = trainer.train(samples, targets);
    }

    /**
     * Vorhersage treffen zu gegebenem Wert.
     */
    const sample_type predict(const dlib::matrix<sample_type, spalten, zeilen> value) const {
        sample_type p = decision_function(value);
        return p;
    }


private:
    std::vector<dlib::matrix<sample_type, spalten, zeilen>> samples;
    std::vector<sample_type>                                targets;
    dlib::krr_trainer<kernel>                               trainer;
    dlib::decision_function<kernel>                         decision_function; //Entscheidungsfunktion


};