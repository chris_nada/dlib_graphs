//
// Created by krizchri on 13.09.2017.
//

#pragma once

#include <iostream>
#include <vector>
#include <dlib/clustering.h>
#include <dlib/rand.h>
#include "ki.h"

/**
 * Maschinenlerntyp:
 * Klassifikation: Kernelized k-means clustering
 * - teilt Daten auf auf k vom Algorithmus erkannten Haufen.
 * - Anzahl der Haufen wird vom Konstruktor bestimmt.
 *
 * @tparam sample_type Datentyp der Werte aus der Datenmatrix. In aller Regel ``double``.
 * @tparam spalten Anzahl der Spalten in der Datenmatrix.
 * @tparam zeilen Anzahl der Zeilen in der Datenmatrix.
 */
template<typename sample_type, const long spalten, const long zeilen>
class KI_C_KKMC : private KI {


public:

    /**
     * Konstruktor für die *kernelized k-means clustering*-KI.
     * @param n_centers Anzahl der zu bildenen Klassen.
     * @param n_vectors Maximale Anzahl der zu nutzenden Vektoren.
     */
    KI_C_KKMC(const unsigned long n_centers, const unsigned int n_vectors = 8)
            : kkmeans(kc) {
        this->n_centers = n_centers;

        // b) Genauigkeit
        // c) maximale Anzahl Vektoren
        kc = dlib::kcentroid<dlib::radial_basis_kernel<dlib::matrix<sample_type, spalten, zeilen>>>
                (dlib::radial_basis_kernel<dlib::matrix<sample_type, spalten, zeilen>>(0.1), 0.01, n_vectors);

        //Algorithmus initialisieren
        kkmeans.set_kcentroid(kc);

    }

    /**
     * Sagt Kategorie voraus vom gegebenen Punkt.
     */
    const unsigned long predict(const dlib::matrix<sample_type, spalten, zeilen> sample) {
        add_learn_data(sample);
        train();
        return kkmeans(samples[samples.size()-1]);
    }

    /**
     * Fügt dem Lernkorpus einen Punkt hinzu.
     */
    void add_learn_data(dlib::matrix<sample_type, spalten, zeilen> sample) { samples.push_back(sample); }

    /**
     * Sagt, zu welcher Kategorie Punkt n aus den Lerndaten gehört.
     */
    const unsigned long predict(const unsigned long n) const { return kkmeans(samples[n]); }

    /**
     * Trainiert die KI.
     */
    void train() override {
        std::cout << "KI_KKMC.train()... ";

        //Trainingsdaten laden
        std::vector<dlib::matrix<sample_type, spalten, zeilen>> initial_centers;

        //Anzahl zu findender Haufen
        kkmeans.set_number_of_centers(n_centers);
        dlib::pick_initial_centers(n_centers, initial_centers, samples, kkmeans.get_kernel());

        //Training starten
        kkmeans.train(samples, initial_centers);
        std::cout << "fertig." << std::endl;
    }

    /**
     * Getter für den Lernkorpus.
     */
    const std::vector<dlib::matrix<sample_type, spalten, zeilen>>& get_samples() const { return samples; };

private:

    unsigned long                                                 n_centers;
    std::vector
            <dlib::matrix<sample_type, spalten, zeilen>>          samples; //Trainingskorpus
    dlib::kcentroid
            <dlib::radial_basis_kernel
                    <dlib::matrix<sample_type, spalten, zeilen>>> kc;
    dlib::kkmeans
            <dlib::radial_basis_kernel
                    <dlib::matrix<sample_type, spalten, zeilen>>> kkmeans;

};
