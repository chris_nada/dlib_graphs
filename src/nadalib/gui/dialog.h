//
// Created by nada on 15.10.2017.
//
#pragma once

#include <string>
#include <vector>
#include "../gfx/gfx.h"
#include "textarea.h"
#include "button.h"

class Dialog {

public:

    /**
     * Zeigt ein maximum von 40 Optionsknöpfen an.
     * Verwendet eine eigene SDL-Eventschleife.
     * @param text Dialoganzeigetext. Darf zirka 4000 Zeichen oder 10 Zeilen lang sein, damit 40 Knöpfe angezeigt werden können.
     * @param options Bis zu 40 Auswahloptionen als Vector von Strings.
     * @return uint8_t der ausgewählten Option.
     */
    static uint8_t show(const std::string& text, std::vector<std::string> options = {"OK"}) {
        Textarea textarea((unsigned int)(Gfx::SCREEN_WIDTH*0.01), 40, text, (unsigned int)(Gfx::SCREEN_WIDTH*0.98));

        //Buttons generieren
        if (options.empty()) options = {"OK"};
        std::vector<Button> buttons;
        const int padding = 8;
        const int btn_w = (int)(Gfx::SCREEN_WIDTH*0.98)/4 - padding/2;
        const int btn_h = 30;
        int x = textarea.get_rect().x;
        int y = textarea.get_rect().y + textarea.get_rect().h + padding * 2;
        for (uint8_t i = 0; i < options.size(); ++i) {
            buttons.emplace_back(x, y, btn_w, btn_h, options[i]);
            y += btn_h + padding;
            if (y + btn_h + padding >= Gfx::SCREEN_HEIGHT) {
                y = textarea.get_rect().y + textarea.get_rect().h + padding * 2;
                x += btn_w + padding;
            }
        }

        Uint32 timer;
        uint16_t delay;
        SDL_Event event;
        while (true) {
            //Rendern
            timer = SDL_GetTicks();
            SDL_RenderClear(Gfx::RENDERER);
            textarea.render();
            for (Button& button : buttons) button.render();
            SDL_RenderPresent(Gfx::RENDERER);

            //Clickhandling
            while (SDL_PollEvent(&event)) {
                switch (event.type) {
                    case SDL_MOUSEBUTTONDOWN:
                    for (uint8_t i = 0; i < buttons.size(); ++i) {
                        if (buttons[i].clicked(&event)) return i;
                    }
                    default:break;
                }
            }
            /* FPS Management */
            delay = 15 - (SDL_GetTicks() - timer);
            if (delay < 1000) SDL_Delay(delay);
        }
    }

    /**
     * Öffnet ein Beispieldialogfenster mit dem Maximum an Optionsknöpfen und einem 10 Zeilen langen Lorem Ipsum.
     */
    static void test() {
        std::vector<std::string> options;
        for (uint8_t i = 1; i <= 40; ++i) options.push_back("Option " + std::to_string(i));
        std::cout << "Option "
                  << (unsigned int)Dialog::show("Voluptas ad eius voluptates labore reiciendis. Quaerat accusamus non quisquam architecto natus mollitia ab occaecati. Vel accusantium dignissimos tempore quia. Corrupti ab id a quod odit dolor. Minima nostrum quia ut est quo nesciunt. Sunt commodi et dolor iure at sit dolores dolores.\n"
                                  "Reprehenderit inventore repellat nemo aut animi. Vel officia nemo aut delectus placeat et eius officiis. Et impedit quis et consectetur et distinctio.\n"
                                  "Ut magni maxime error. Voluptate et porro repellendus reiciendis aut quos quia. Cupiditate aspernatur sunt voluptatibus culpa adipisci error quisquam.\n"
                                  "A aut id perferendis totam corporis iste ipsum. Architecto voluptas harum laborum. Corporis dicta tempora odit nostrum quia. Ab officia eos aut distinctio vel et nesciunt magnam. Est ipsam nam molestias aut facilis in.\n"
                                  "Aut nihil aperiam alias officiis quibusdam. Ipsa et voluptates quia quia voluptates. Quis omnis nemo autem. Quibusdam dolorem neque eum qui iusto sapiente alias impedit. Dolore enim id illum repudiandae pariatur.\n"
                                  "Ut doloribus accusamus et inventore provident ad. Eum maxime placeat id assumenda et mollitia et minima. Reprehenderit impedit debitis non ad deleniti sit ea.\n"
                                  "Atque corrupti mollitia quam consequuntur laudantium. Velit ipsam quos deleniti delectus. Sint illum qui nihil odit veritatis non. Earum vitae reprehenderit architecto.\n"
                                  "Ut reiciendis reiciendis maxime sequi accusantium quia voluptatem. Inventore et mollitia culpa et autem vel quia vel. Saepe et quo natus. Consequatur magni et qui. Ut sit dolores iste ipsam quod delectus.\n"
                                  "Autem optio est amet nesciunt fuga nam. Ut amet sequi quis consequatur illum quae. At nostrum aspernatur occaecati. Sit excepturi rerum repellendus non esse eum. Ab nisi enim et. Inventore at blanditiis rerum nisi.\n"
                                  "Labore sed eaque animi natus ut est doloremque. Aut tenetur nostrum sunt et aperiam aut consequatur et. Dolor et et tempore vitae vel sunt non eligendi. Labore enim sint consequatur sint. Qui non velit suscipit laborum omnis aliquid delectus. Eum enim accusantium consequuntur aut.",
                                  options)
                  << " selected" << std::endl;
    }

private:

    Dialog() = default;

};