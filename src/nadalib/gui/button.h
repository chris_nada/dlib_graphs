#pragma once

#include <string>
#include <SDL_render.h>
#include "../gfx/gfx.h"
#include "node.h"

class Button : public Node {

public:
    ///ctor Default
    Button(int x, int y, const std::string& text, bool active = true);

    ///ctor + size
    Button(int x, int y, uint16_t width, uint16_t height, const std::string& text, bool active = true);

    void render() const override;

    void set_text(const std::string& text) { this->text = text; }

    bool mouse_over() const;

    bool clicked(const SDL_Event* event) const;

    bool mouse_wheel_up(const SDL_Event* event) const;

    bool mouse_wheel_down(const SDL_Event* event) const;


protected:
    std::string text;


};
