//
// Created by nada on 12.10.2017.
//
#include "input_text.h"


Textinput::Textinput(int x, int y, std::string& text) : text(text) {
    rect.x = x;
    rect.y = y;
    rect.w = 240;
    rect.h = 40;
    input  = "";
    size   = 12;
    render();
}

Textinput::Textinput(int x, int y, uint16_t width, uint16_t height, std::string& text, uint8_t size) : text(text) {
    rect.x = x;
    rect.y = y;
    rect.w = width;
    rect.h = height;
    input  = "";
    this->size = size;
    render();
}

void Textinput::update(const SDL_Event* event) {
    if (event->type == SDL_KEYDOWN) {
        int mx, my;
        SDL_GetMouseState(&mx, &my);
        if (mx < rect.x) return;
        if (mx > rect.x + rect.w) return;
        if (my < rect.y) return;
        if (my > rect.y + rect.h) return;
        std::string temp(SDL_GetKeyName(event->key.keysym.sym));
        if (input.size() < size && temp.size()==1) {
            SDL_Keymod modstates = SDL_GetModState();
            if (modstates & KMOD_SHIFT) { }
            else std::transform(temp.begin(), temp.end(), temp.begin(), ::tolower);
            input += temp;
            render();
        }
        if (input.size()>0 && event->key.keysym.sym == SDLK_BACKSPACE) {
            input.erase(input.size()-1);
            render();
        }

    }
}

void Textinput::render() const {
    SDL_SetRenderDrawColor( Gfx::RENDERER, Gfx::RGB_BG[0], Gfx::RGB_BG[1], Gfx::RGB_BG[2], 0xFF );
    SDL_RenderFillRect( Gfx::RENDERER, &rect );
    SDL_SetRenderDrawColor( Gfx::RENDERER, 0x00, 0x00, 0x00, 0xFF );
    Gfx::blit_text(text + input, rect.x + 12, rect.y + rect.h / 2 - 8); //TODO ineffektiv
}
