//
// Created by nada on 12.10.2017.
//
#include "button.h"

Button::Button(int x, int y, const std::string& text, bool active)
        : Button(x, y, 160, 40, text) {
    Button::active = active;
}

Button::Button(int x, int y, uint16_t width, uint16_t height, const std::string& text, bool active)
        : text(text) {
    Button::active = active;
    rect.x = x;
    rect.y = y;
    rect.w = width;
    rect.h = height;
    render();
}

void Button::render() const {
    if (active) {
        if (mouse_over())
            SDL_SetRenderDrawColor(Gfx::RENDERER, Gfx::RGB_BG2[0], Gfx::RGB_BG2[1], Gfx::RGB_BG2[2], 0xFF);
        else SDL_SetRenderDrawColor(Gfx::RENDERER, Gfx::RGB_BG[0], Gfx::RGB_BG[1], Gfx::RGB_BG[2], 0xFF);
    }
    else {
        SDL_SetRenderDrawColor(Gfx::RENDERER, 0x1F, 0x0F, 0x0F, 0xFF);
    }
    SDL_RenderFillRect(Gfx::RENDERER, &rect);
    Gfx::blit_text(text, rect.x + 12, rect.y + rect.h / 2 - 8); //TODO durch Label ersetzen
    SDL_SetRenderDrawColor( Gfx::RENDERER, 0x00, 0x00, 0x00, 0xFF );
}

bool Button::mouse_over() const {
    int mx, my;
    SDL_GetMouseState(&mx, &my);
    if (mx < rect.x) return false;
    else if (mx > rect.x + rect.w) return false;
    else if (my < rect.y) return false;
    else if (my > rect.y + rect.h) return false;
    return true;
}

bool Button::clicked(const SDL_Event* event) const {
    if (event->type == SDL_MOUSEBUTTONDOWN && active) {
        return mouse_over();
    }
    return false;
}

bool Button::mouse_wheel_up(const SDL_Event* event) const {
    if (event->wheel.y == 1) {
        int mx, my;
        SDL_GetMouseState(&mx, &my);
        if (mx < rect.x) return false;
        if (mx > rect.x + rect.w) return false;
        if (my < rect.y) return false;
        if (my > rect.y + rect.h) return false;
        return true;
    }
    return false;
}

bool Button::mouse_wheel_down(const SDL_Event* event) const {
    if (event->wheel.y == -1) {
        int mx, my;
        SDL_GetMouseState(&mx, &my);
        if (mx < rect.x) return false;
        if (mx > rect.x + rect.w) return false;
        if (my < rect.y) return false;
        if (my > rect.y + rect.h) return false;
        return true;
    }
    return false;
}
