//
// Created by nada on 10.10.2017.
//
#include "commons.h"

std::vector<std::string> Commons::tokenize(const std::string& text, char token) {
    std::vector<std::string> teile;
    unsigned long long anfang = 0;
    unsigned long long ende = 0;
    while ((ende = text.find(token, anfang)) != std::string::npos) {
        teile.push_back(text.substr(anfang, ende - anfang));
        anfang = ende;
        anfang++;
    }
    teile.push_back(text.substr(anfang));
    return teile;
}

bool Commons::replace(std::string& string, const std::string& alt, const std::string& neu) {
    if(string.find(alt) == std::string::npos) return false;
    string.replace(string.find(alt), alt.length(), neu);
    return true;
}

bool Commons::replace_all(std::string& string, const std::string& alt, const std::string& neu) {
    if(alt.empty()) return false;
    if(string.find(alt) == std::string::npos) return false;
    size_t start_pos = 0;
    while((start_pos = string.find(alt, start_pos)) != std::string::npos) {
        string.replace(start_pos, alt.length(), neu);
        start_pos += neu.length();
    }
    return true;
}

bool Commons::is_float(const std::string& float_string) {
    std::istringstream iss (float_string);
    float f;
    iss >> std::noskipws >> f;
    return iss.eof() && !iss.fail();
}

const std::string Commons::substring(const std::string& such_string, char start, char ende) {
    return such_string.substr(
            such_string.find(start),
            such_string.find(ende) - such_string.find(start)
    );
}

const std::string Commons::to_lower(const std::string& mixed_case_string) {
    std::string lower;
    std::locale locale;
    for (char c : mixed_case_string) {
        lower += std::tolower(c, locale);
    }
    return lower;
}
