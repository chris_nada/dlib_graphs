//
// Created by krizchri on 06.09.2017.
//
#pragma once

#include <dirent.h>
#include <windows.h>
#include <algorithm>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>

/**
 * Enthält statische Methoden zum Umgang mit Dateien und Ordnern.
 */
class Filesystem {

public:

    /**
     * Liefert alle Dateien in einem Verzeichnis zurück als std::vector<std::string>.
     * @param folder Zu durchsuchende Ordner.
     * @param wenn gegeben, dann werden nur Dateien mit dieser Dateiendung gesucht. Default = *
     */
    static std::vector<std::string> get_files(const std::string& folder, const std::string& extension = "*.*");

    /**
     * Liefert alle Unterordner in einem Verzeichnis zurück als std::vector<std::string>.
     * @param folder Zu durchsuchende Ordner.
     */
    static std::vector<std::string> get_subfolders(const std::string& folder);

    /**
     * Löscht aus einem Verzeichnis alle Dateien.
     * @param folder Ordner, in dem alle Dateien gelöscht werden.
     */
    static void clean_folder(const std::string& folder);

    /**
     * Erstellt einen Ordner.
     * @param folder Verzeichnispfad. Kann relativ oder absolut sein.
     * @return true, wenn der Ordner tatsächlich erstellt wurde
     */
    static bool create_directory(const std::string& folder);

    /**
     * Lösche einen Ordner.
     * @param folder Verzeichnispfad. Kann relativ oder absolut sein.
     * @return true, wenn der Ordner tatsächlich gelöscht wurde in der Methode
     */
    static bool delete_directory(const std::string& folder);

    /**
     * Kopiert eine Datei. Gepuffert.
     * @param from  Quelldatei
     * @param to    Zielpfad (einschließlich Dateiname und -endung)
     */
    static void copy_file(const std::string& from, const std::string& to);

};