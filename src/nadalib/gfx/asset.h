//
// Created by nada on 03.10.2017.
//
#pragma once

#include <SDL_rect.h>
#include <string>
#include <SDL_image.h>
#include <iostream>

class Asset {

public:

    explicit Asset(std::string& path);

    virtual void render();

    virtual void render(int x, int y);

    virtual ~Asset();

private:
    SDL_Texture* texture;
    SDL_Rect r1;
    SDL_Rect r2;

};