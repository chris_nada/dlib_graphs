#pragma once

#include <SDL_surface.h>
#include <string>
#include <SDL_render.h>
#include <SDL_ttf.h>
#include <vector>
#include <map>
#include <memory>
#include "asset.h"

class Gfx {

public:
    static unsigned short SCREEN_WIDTH;
    static unsigned short SCREEN_HEIGHT;
    static SDL_Window*   WINDOW;
    static SDL_Renderer* RENDERER;
    static TTF_Font* FONT_LABEL;
    static TTF_Font* FONT_SANS;
    static TTF_Font* FONT_MONO;
    static TTF_Font* FONT_TEXT;
    static TTF_Font* FONT_TINY;
    static uint8_t RGB_BG[];  //Node Hintergrundfarbe
    static uint8_t RGB_BG2[]; //Node Farbe ausgewählt

    /* --- Blit Methoden --- */

    static void blit(const std::string& path, const int x, const int y);
    static void blit(SDL_Texture* texture, const SDL_Rect* r1, const SDL_Rect* r2);
    static void blit_fullscreen(const std::string& path);
    static void blit_sized(const std::string& path, const int x, const int y, const unsigned int max_size, bool center = true);
    static void blit_sized(const std::string& path, const int x, const int y, const unsigned int max_size, const uint8_t transparency);
    static void blit_text(const std::string&, const int x, const int y, TTF_Font* = FONT_LABEL, const SDL_Color font_color = {255, 255, 255, 255});
    static void blit_rect(const int x, const int y, const int w, const int h, const bool black = true);
    static void blit_rect(const int x, const int y, const int w, const int h, uint8_t r, uint8_t g, uint8_t b);
    static void blit_asset(const std::string& path, const int x, const int y);

    ///dtor
    static void finalize();

protected:

    static std::map<std::string, std::unique_ptr<Asset>> assets;

private:
    Gfx() = default;

};
