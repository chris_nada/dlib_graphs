#pragma once

#include <iostream>
#include <gtkmm/button.h>
#include <gtkmm/window.h>
#include <gtkmm/grid.h>
#include <gtkmm/textview.h>
#include <SDL_pixels.h>
#include "nadalib/gfx/utils.h"
#include "nadalib/common/commons.h"
#include "nadalib/ki/ki_od_mmod.h"
#include "nadalib/ki/ki_od_hog.h"
#include "nadalib/ki/ki_c_kkmc.h"
#include "nadalib/gfx/gfx.h"


class Hauptmenu : public Gtk::Window {

public:

    Hauptmenu()
            : grid(),
              button1("show_plot()"),
              button2("ki_hog test"),
              button3("ki_dnn test"),
              button4("test_plot()"),
              button5("refresh()"),
              button6("   ") {
        std::cout << "Hauptmenu()" << std::endl;
        set_title("gtk window");
        set_border_width(0x0F);

        add(grid);
        grid.set_column_spacing(0x0F);
        grid.set_row_spacing(   0x0F);

        button1.signal_clicked().connect(sigc::mem_fun(*this, &Hauptmenu::show_plot));
        button2.signal_clicked().connect(sigc::mem_fun(*this, &Hauptmenu::detection_hog));
        button3.signal_clicked().connect(sigc::mem_fun(*this, &Hauptmenu::detection_dnn));
        button4.signal_clicked().connect(sigc::mem_fun(*this, &Hauptmenu::test_plot));
        button4.signal_clicked().connect(sigc::mem_fun(*this, &Hauptmenu::action1));

        entry_values.set_size_request(320, 400);
        entry_values.set_monospace(true);

        grid.attach(button1,      0, 0, 1, 1);
        grid.attach(button2,      1, 0, 1, 1);
        grid.attach(button3,      0, 1, 1, 1);
        grid.attach(button4,      1, 1, 1, 1);
        grid.attach(button5,      0, 2, 2, 1);
        grid.attach(entry_values, 0, 3, 2, 4);


        show_all_children();

    }

    virtual ~Hauptmenu() = default;


private:

    void detection_hog() {
        std::cout << "detection_hog()" << std::endl;
        KI_OD_HOG od("data/hog/training.xml", "data/hog/testing.xml", false);
        od.train();
        std::cout << "detection() fertig." << std::endl;
    }

    void detection_dnn() {
        std::cout << "detection_dnn()" << std::endl;
        KI_OD_MMOD od("data/dnn/training.xml", "data/dnn/testing.xml");
        od.train();
        std::cout << "detection() fertig." << std::endl;
    }

    void action1() {
        std::cout << "refresh()" << std::endl;
        SDL_RenderPresent(Gfx::RENDERER);
    }

    void show_plot() {
        std::cout << "show_plot()" << std::endl;
        std::string text = entry_values.get_buffer()->get_text(true);
        std::vector<std::string> zeilen = Commons::tokenize(text, '\n');
        std::vector<std::tuple<double, double, SDL_Color>> values;
        for (const std::string& zeile : zeilen) {
            std::vector<std::string> paar = Commons::tokenize(zeile, ' ');
            if (paar.size() == 2) {
                try {
                    values.push_back(std::make_tuple<double, double, SDL_Color>(
                            std::stod(paar[0]), std::stod(paar[1]),
                            {0x00, 0x00, 0x00, 0xFF})
                    );
                }
                catch (const std::exception& e) {
                    std::cerr << e.what() << std::endl;
                }
            }
        }

        //Werte Clustern
        double anzahl_cluster;
        std::cout << "Anzahl Cluster = " << std::flush;
        std::cin  >>  anzahl_cluster;
        std::clamp(anzahl_cluster, 2.0, 25.0);
        KI_C_KKMC<double, 2, 1> k_means(static_cast<const unsigned int>(std::round(anzahl_cluster)), 20);
        for (std::tuple<double, double, SDL_Color> tupel : values) {
            dlib::matrix<double, 2, 1> sample;
            sample(0) = std::get<0>(tupel);
            sample(1) = std::get<1>(tupel);
            k_means.add_learn_data(sample);
        }
        k_means.train();
        for (unsigned long l = 0; l < values.size(); ++l) {
            double cluster = k_means.predict(l);
            switch ((int) std::round(cluster)) {
                case 0:
                    std::get<SDL_Color>(values[l]) = {0x00, 0xFF, 0x00, 0xFF};
                    break;
                case 1:
                    std::get<SDL_Color>(values[l]) = {0xFF, 0x00, 0x00, 0xFF};
                    break;
                case 2:
                    std::get<SDL_Color>(values[l]) = {0x00, 0x00, 0xFF, 0xFF};
                    break;
                case 3:
                    std::get<SDL_Color>(values[l]) = {0xFF, 0xFF, 0x00, 0xFF};
                    break;
                case 4:
                    std::get<SDL_Color>(values[l]) = {0x00, 0xFF, 0xFF, 0xFF};
                    break;
                case 5:
                    std::get<SDL_Color>(values[l]) = {0x80, 0x80, 0x00, 0xFF};
                    break;
                case 6:
                    std::get<SDL_Color>(values[l]) = {0x40, 0x40, 0x80, 0xFF};
                    break;
                default:
                    std::get<SDL_Color>(values[l]) = {0x40, 0x80, 0x40, 0xFF};
                    break;
            }
        }

        //Anzeigen
        if (values.size() > 1) Utils::draw_dot_cloud(values);
    }

    void test_plot() {
        std::cout << "test_plot()" << std::endl;
        std::vector<std::tuple<double, double, SDL_Color>> values;
        for (double x = -50; x < 50; ++x) {
            values.push_back(std::make_tuple<double, double, SDL_Color>((double)x, 0.01*x*x*x, {0x00, 0x00, 0x00, 0xFF}));
        }
        Utils::draw_dot_cloud(values);
    }

    /* Felder */
    Gtk::Grid grid;
    Gtk::Button button1;
    Gtk::Button button2;
    Gtk::Button button3;
    Gtk::Button button4;
    Gtk::Button button5;
    Gtk::Button button6;
    Gtk::TextView entry_values;

};